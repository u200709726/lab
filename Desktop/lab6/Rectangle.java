package com.lab6;

public class Rectangle {
    int sideA, sideB;
    Point topLeft;
    public Rectangle(int sidea, int sideb, Point topleft) {
        sideA = sidea;
        sideB = sideb;
        topLeft = topleft;
    }

    public int area() {
        return sideA * sideB;
    }

    public int perimeter() {
        return 2 * (sideA + sideB);
    }

    public Point[] corners() {
        Point corners[] = new Point[4];
        Point topRight = new Point(topLeft.xCoord + sideA, topLeft.yCoord);
        Point bottomLeft = new Point(topLeft.xCoord, topLeft.yCoord - sideB);
        Point bottomRight = new Point(topLeft.xCoord + sideA, topLeft.yCoord - sideB);
        corners[0] = topLeft;
        corners[1] = topRight;
        corners[2] = bottomLeft;
        corners[3] = bottomRight;
        return corners;
    }


}
