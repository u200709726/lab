package com.lab6;

public class Main {

    public static void main(String[] args) {
        int sideA = 10;
        int sideB = 12;
        Point topLeft = new Point(2, 7);
        Rectangle rectangle = new Rectangle(sideA, sideB, topLeft);
        System.out.println("rectangle.area() = " + rectangle.area());
        Point[] points = rectangle.corners();

        System.out.println("Top Left Point Coordinates: "+points[0]);
        System.out.println("Top Right Point Coordinates: "+points[1]);
        System.out.println("Bottom Left Point Coordinates: "+points[2]);
        System.out.println("Bottom Right Point Coordinates: "+points[3]);

    }
}
