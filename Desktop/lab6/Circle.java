package com.lab6;

public class Circle {
    int radius;
    Point center;
    public Circle(int rad, Point cen) {
        radius = rad;
        center = cen;
    }
    public double area() {
        return Math.PI * radius * radius;
    }

    public double perimeter() {
        return 2.0 * Math.PI * radius;
    }

    public boolean intersect(Circle circle) {
        int x1 = this.center.xCoord, y1 = this.center.yCoord;
        int x2 = circle.center.xCoord, y2 = circle.center.yCoord;
        int D_square = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
        int radiiSum_square = (this.radius + circle.radius) * (this.radius + circle.radius);

        if (D_square <= radiiSum_square)
            return false;
        return true;
    }
}
