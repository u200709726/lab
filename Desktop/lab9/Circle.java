package com.shabe2d;

public class Circle {
    private int radius;



    public Circle(int radius) {
        this.radius = radius;
    }


    public double area(){
        return Math.PI * radius * radius;
    }


    @Override
    public String toString(){
        return "radius =" + radius;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj instanceof Circle) {
            return radius == c.radius;

        }

    }

}
