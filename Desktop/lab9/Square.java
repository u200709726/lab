package com.shabe2d;

public class Square {
    private int side;

    public Square(int side) {
        this.side = side;
    }

    public int area() {
        return side * side;

    }
    @Override
    public String toString(){
        return "side = " + side;



    }
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj.getClass().equals(this.getClass()) {
            Square c = (Square) obj;
            return side == c.side;

        }
        return false;

    }
}
