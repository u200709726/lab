package com.lap3;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber3 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        int no = rand.nextInt(100);

        int num;


        while( true ) {
            System.out.println("Hi! I'm thinking of a number between 0 and 99.");
            System.out.print("Can you guess it: ");
            num = in.nextInt();
            if (num == no)
                System.out.println("Congratulation!");
            else
                System.out.println("Sorry the number was: " + no);
            break;
        }


    }
}
