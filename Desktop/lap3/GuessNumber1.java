package com.lap3;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");
        int guess = in.nextInt();
        int num = rand.nextInt(5);

        if (guess == num)
            System.out.println("Conguratulation!");
        else
            System.out.println("Sorry the number was: " + num);


    }
}
